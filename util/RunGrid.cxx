#include "TH1.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "TEnv.h"
#include "TFile.h"
#include "TString.h"
#include "TVectorD.h"

#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"

#include "TruthPhotonAnalysis/photonAnalysis.h"
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AnaAlgorithm/AnaAlgorithmConfig.h>

using namespace std;

int main( int argc, char* argv[] ) {
  TString configFile;
  int ip=1;
  while (ip<argc) {
    
    if (std::string(argv[ip]).substr(0,2)=="--") {
      
      // config file                                                                                        
      if (std::string(argv[ip])=="--config") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          configFile=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno config file inserted"<<endl; break;}
      }
    }
  }

  // Get Config file                                                                                       
  TEnv * settings = new TEnv();
  int status = settings->ReadFile("share/configFile",EEnvLevel(0));
  if (status!=0) {
    cout<<"cannot read config file"<<endl;
    cout<<"******************************************\n"<<endl;
    return 1;
  }
  string submitDir = settings->GetValue("OutDir","subDir");
  string InputDir  = settings->GetValue("GridInputDir","");
  string outName   = settings->GetValue("GridOutName", "");
  delete settings;
  xAOD::Init().ignore();

  // Construct the samples to run on:                                                                      
  SH::SampleHandler sh;
  cout << "Looking for: " << InputDir << endl;
  SH::scanDQ2 (sh, InputDir );  //mc14_8TeV.117050.PowhegPythia_P2011C_ttbar.recon.AOD.e1727_s1933_s1911_r5591/");                                                                                                    
   //SH::scanRucio(sh, InputDir );                                                                          


   // Set the name of the input TTree. It's always "CollectionTree"                                         
   // for xAOD files.                                                                                       
   sh.setMetaString( "nc_tree", "CollectionTree" );

   // Print what we found:                                                                                  
   sh.print();

   // Create an EventLoop job:                                                                              
   EL::Job job;

   job.sampleHandler( sh );

   // Set to delete output folder if it exists                                                              
   // Dangerous setting, only on for testing purposes                                                       
   job.options()->setDouble (EL::Job::optRemoveSubmitDir, 1);
   job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

   // Add our analysis to the job:                                                                          

   EL::AnaAlgorithmConfig alg;
   alg.setType ("photonAnalysis");

   // set the name of the algorithm (this is the name use with
   // messages)
   alg.setName ("AnalysisAlg");
   job.algsAdd( alg );

   // Run the job using the local/direct driver:                                                            
   EL::PrunDriver driver;
   driver.options()->setString("nc_outputSampleName", outName); //user.ahorton.April2.%in:name[2]%"); 
   driver.submitOnly( job, submitDir );
return 0;
}
