#include <iostream>
//#include <filesystem>                                                         
#include <iomanip>
#include <string>
#include "TEnv.h"
#include "TFile.h"
#include "TString.h"
#include "TVectorD.h"
#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "TruthPhotonAnalysis/photonAnalysis.h"
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AnaAlgorithm/AnaAlgorithmConfig.h>

int main( int argc, char* argv[] ) {

  //   EventFilter TheFilter;                                                                                 
  // Reads in the configurationg file supplied using --config X                                            
  // I still cannot figure out how to pass this information on                                             
  // to the workers, I may give up and use a work around                                                   
  // /////////////////////////////////////////////////////////                                             

  TString configFile;
  int ip=1;
  while (ip<argc) {

    if (std::string(argv[ip]).substr(0,2)=="--") {

      // config file                                                                                        
      if (std::string(argv[ip])=="--config") {
        if (ip+1<argc && std::string(argv[ip+1]).substr(0,2)!="--") {
          configFile=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno config file inserted"<<std::endl; break;}
      }
    }
  }

  // Get Config file                                                                                       
  TEnv * settings = new TEnv();
  int status = settings->ReadFile("share/configFile",EEnvLevel(0));
  if (status!=0) {
    std::cout<<"cannot read config file"<<std::endl;
    std::cout<<"******************************************\n"<<std::endl;
    return 1;
  }
  std::string submitDir = settings->GetValue("OutDir","subDir");
  std::string InputDir  = settings->GetValue("LocalInputDir","");
  delete settings;

  xAOD::Init().ignore();

  // Construct the samples to run on:                                                                      
  SH::SampleHandler sh;
  SH::scanDir( sh, InputDir ); //"/afs/cern.ch/atlas/project/PAT/xAODs/r5597/" );                          

  // Set the name of the input TTree. It's always "CollectionTree"                                         
  // for xAOD files.                                                                                       
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:                                                                                  
  sh.print();

  // Create an EventLoop job:                                                                              
  EL::Job job;

  job.sampleHandler( sh );

  //job.options()->setDouble (EL::Job::optMaxEvents, 10000);                                               

  // Set to delete output folder if it exists                                                              
  // Dangerous setting, only on for testing purposes                                                       
  job.options()->setDouble (EL::Job::optRemoveSubmitDir, 1);
  //   job.options()->setString(EL::Job::optXaodAccessMode,EL::Job::optXaodAccessMode_branch);                
  job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

  // Add our analysis to the job:                                                                          
  EL::AnaAlgorithmConfig alg;
  alg.setType ("photonAnalysis");

  // set the name of the algorithm (this is the name use with
  // messages)
  alg.setName ("AnalysisAlg");
  job.algsAdd( alg );

  // Run the job using the local/direct driver:                                                            
  EL::DirectDriver driver;
  driver.submit( job, submitDir );
  return 0;
}
