#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TruthPhotonAnalysis/photonAnalysis.h>

// this is needed to distribute the algorithm to the workers
// Infrastructure include(s):
#include "PathResolver/PathResolver.h"

// Truth xAOD include(s):
#include "xAODTruth/TruthParticle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthVertex.h"

//FastJet:
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/ClusterSequenceAreaBase.hh"

// Systematics include(s):
#include "PATInterfaces/SystematicVariation.h"
#include <AsgTools/MessageCheck.h>
// for smart slimming
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH2.h"
#include "TH1.h"
#define MySafeDelete(x) { if (x) { delete x; x=NULL; } }


//---------------CONSTRUCTOR--------------//
photonAnalysis :: photonAnalysis (const std::string& name,
				   ISvcLocator *pSvcLocator)
				  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize()
  std::cout<<"this is the initialization"<<std::endl;
  // xAOD containers
  m_event = 0;
  m_eventinfo = 0;
  m_central_eventShape_truth = 0;
  m_forward_eventShape_truth = 0;
  m_egamma_truth = 0;


  //  std::string outputTruthName="dummytest";
  double max_true=3000;                                                                                                                                       
  double min_true=25;                                                                                                                                                          
  double mc_weight=1.0;                                                                                                                                                         
  bool isdata=false;
  double masscut=450;                                                                                                                                                           
  double costcut=0.83;

  // cuts  used at the preselection stage
  min_et = 150.; // GeV
  min_jet_et = 60.; // GeV //for resolution studies
  min_crack = 1.37;
  max_crack = 1.56;
  max_eta = 2.37;

  // event
  m_eventCounter = 0;
  m_isMC = 0;
  m_rho_central_truth = 0;
  m_rho_forward_truth = 0;
}

StatusCode photonAnalysis :: initialize ()
{
  // histograms
  photonAnalysis::SetupHistograms();

  std::cout<<"Empieza inicializacion"<<std::endl;
  m_event = wk()->xaodEvent();
  ANA_CHECK_SET_TYPE (EL::StatusCode)
    Info("initialize()", "Number of events = %lli",m_event->getEntries());

  m_writeTruthTree = true;
  if(m_writeTruthTree)
    {
      std::cout<<" in writeTruth loop "<<std::endl;
      book (TTree ("analysis", "My analysis ntuple"));
      //      TFile *outputTruthTree = wk()->getOutputFile(outputTruthName);
      // truthTree = new TTree("truthTree", "truthTree");
      TTree* truthTree = tree ("analysis");
      //     truthTree->SetDirectory(outputTruthTree);

      //truth Tree                                                              
      truthTree->Branch("ph_isConvTruth",&m_t_isConvTruth);
      truthTree->Branch("ph_isBrem",&m_t_isBrem);
      truthTree->Branch("ph_isoTruth",&m_t_isoTruth);
      truthTree->Branch("ph_ptTruth",&m_t_ptTruth);
      truthTree->Branch("ph_ptParton",&m_t_ptParton);
      truthTree->Branch("ph_etaTruth",&m_t_etaTruth);
      truthTree->Branch("ph_phiTruth",&m_t_phiTruth);
      truthTree->Branch("mc_weight", &m_t_mc_weight);
      truthTree->Branch("mc_allweights", &m_t_mc_allweights);
      truthTree->Branch("RunNumber",&m_t_runnumber);
      truthTree->Branch("EventNumber",&m_t_eventnumber);
      truthTree->Branch("jet_ptTruth1", &m_t_jetpt1);
      truthTree->Branch("jet_eTruth1", &m_t_jete1);
      truthTree->Branch("jet_massTruth1", &m_t_jetmass1);
      truthTree->Branch("jet_etaTruth1", &m_t_jeteta1);
      truthTree->Branch("jet_rapTruth1", &m_t_jetrap1);
      truthTree->Branch("jet_phiTruth1", &m_t_jetphi1);
      truthTree->Branch("jet_ptTruth2", &m_t_jetpt2);
      truthTree->Branch("jet_eTruth2", &m_t_jete2);
      truthTree->Branch("jet_massTruth2", &m_t_jetmass2);
      truthTree->Branch("jet_etaTruth2", &m_t_jeteta2);
      truthTree->Branch("jet_rapTruth2", &m_t_jetrap2);
      truthTree->Branch("jet_phiTruth2", &m_t_jetphi2);

    }
  std::cout<<" End initizlize "<<std::endl;
  return EL::StatusCode::SUCCESS;

}



StatusCode photonAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single               
  // events, e.g. read input variables, apply cuts, and fill                    
  // histograms and trees.  This is where most of your actual analysis          
  // code will go.                         
  const char* APP_NAME = "execute";
  if( (m_eventCounter % 1000) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;
  const double pi = TMath::Pi();
  std::cout<<"Event "<<m_eventCounter<<std::endl;

  photonAnalysis::Initialize_event();
  if(m_writeTruthTree)
    {
      m_t_runnumber = 0.;
      m_t_eventnumber = 0.;
      m_t_mc_weight = 0.;
      m_t_mc_allweights.clear();
      m_t_isConvTruth = 0.;
      m_t_isBrem = 0.;
      m_t_isoTruth = 0.;
      m_t_ptTruth = 0.;
      m_t_etaTruth= 0.;
      m_t_phiTruth = 0.;
      m_t_jetpt1 = 0;
      m_t_jete1 = 0;
      m_t_jetmass1 = 0;
      m_t_jeteta1 = 0;
      m_t_jetrap1 = 0;
      m_t_jetphi1 = 0;
      m_t_jetpt2 = 0;
      m_t_jete2 = 0;
      m_t_jetmass2 = 0;
      m_t_jeteta2 = 0;
      m_t_jetrap2 = 0;
      m_t_jetphi2 = 0;


    }

  //event info                                                                  
  int mirun = m_eventinfo->runNumber();
  Long64_t  mievnt =  m_eventinfo->eventNumber();
  double weight = 1.0;

  //***Weights for SherpaNLO***                                                 
  std::vector< float > weights;
  weights.clear();
  if(m_isMC){
    weights= m_eventinfo->mcEventWeights();

    if(m_eventCounter<10){ std::cout<<"weights[0] "<<weights[0]<<std::endl;}
    if( weights.size() > 0 ){
      m_t_mc_weight = weights[0];

      for(int f = 0; f<=(weights.size()); f++)
        {
          m_t_mc_allweights.push_back(weights[f]);

        }

    }
    //hallweights->Fill(0.5,m_t_mc_weight);                                     
    //hinisum->Fill(0.5,m_t_mc_weight);
    weight = m_t_mc_weight;



  }


  //***TRUTH***                                                                 
  //leading prompt photon features                                              
  double lpphotonet = 0;
  double lpphotonetparton = 0;
  double lpphotone = 0;
  double lpphotoneta = 0;
  double lpphotonabseta = 0;
  double lpphotonphi = 0;
  double lpphotonetisoEDcorr = 0;
  int   lpphotonishard = 0; //hard by default                                   
  int   lpphotonisunconv = 0; //unconv by default                               
  bool pass_truthkin = false;
  float jetptmax=-1.0;
  float jetpt2max=-1.0;
  float ljetpt=-9e9, ljeteta=-9e9, ljetrap = -99, ljetphi=-9e9, ljetm=-9e9, ljete = -9e9;
  float sljetpt=-9e9, sljeteta=-9e9, sljetrap = -99, sljetphi=-9e9, sljetm=-9e9, sljete = -9e9;

  double obs_values_truth[14]={0.0};

  if(m_isMC ){


    //Build own jets.
    
    xAOD::TruthParticleContainer::const_iterator particle_itr = m_truth_particles->begin();
    xAOD::TruthParticleContainer::const_iterator particle_end = m_truth_particles->end();
    
    std::vector <fastjet::PseudoJet> fjInputs;
    fjInputs.resize(0);
    for( particle_itr = m_truth_particles->begin(); particle_itr != particle_end; ++particle_itr ) {
      // if( ((*particle_itr)->status())==1 && abs((*particle_itr)->pdgId())<100 && (*particle_itr)->pt()/100. <20){
      if( ((*particle_itr)->status())==1 ){
	//std::cout<<" Particles "<<(*particle_itr)->pdgId()<< "  "<<(*particle_itr)->pt()<<std::endl;
      }
	if( ((*particle_itr)->status())==1 && (*particle_itr)->barcode()<200000 ){
	  int idpart=abs(((*particle_itr)->pdgId()));
	  bool munu=false;
	  if(idpart==12 || idpart==13 || idpart==14 || idpart==16){munu=true;}
	  double partpx=double(((*particle_itr)->px()));
	  double partpy=double(((*particle_itr)->py()));
	  double partpz=double(((*particle_itr)->pz()));
	  double parte =double(((*particle_itr)->e()));
	  if(fabs(((*particle_itr)->eta()))<5 && !munu){
	    fjInputs.push_back(fastjet::PseudoJet(partpx,partpy,partpz,parte));
	  }
	}
      }
    // Run Fastjet algorithm without munu                                                                                                                        
    //fastjet                                                                                                                                                        
    double Rparam = 0.4;                                                                                                                                             
    fastjet::Strategy               strategy = fastjet::Best;                                                                                                        
    fastjet::RecombinationScheme    recombScheme = fastjet::E_scheme;                                                                                                
    fastjet::JetDefinition  jetDef=fastjet::JetDefinition(fastjet::antikt_algorithm, Rparam, recombScheme, strategy);                                                
                                                                                                                                                                     
    std::vector <fastjet::PseudoJet> inclusiveJets, sortedJets; inclusiveJets.clear(); sortedJets.clear();                                                           
    fastjet::ClusterSequence clustSeq(fjInputs, jetDef);                                                                                                             
                                                                                                                                                                     
    inclusiveJets = clustSeq.inclusive_jets(5000.0);                                                                                                                 
    sortedJets    = sorted_by_pt(inclusiveJets);                                                                                                                     
    if(m_eventCounter==1){                                                                                                                                           
    std::cout << " jetdef and area_def " << std::endl;                                                                                                             
    std::cout << jetDef.description() << std::endl;                                                                                                                
    }                                                                                                                                                                
    // loop sobre jets (without munu)                                                                                                



    bool hayph = false;
    
    
    xAOD::TruthParticleContainer::const_iterator truth_part = m_truth_particles->begin();
    xAOD::TruthParticleContainer::const_iterator truth_part_end = m_truth_particles->end();
    xAOD::TruthParticleContainer::const_iterator particle_itr_lpphoton=truth_part;
    for(  truth_part = m_truth_particles->begin(); truth_part != truth_part_end; ++truth_part ) {
      const xAOD::TruthParticle * particle =(*truth_part);
      bool IsPromptph = false;
      IsPromptph = photonAnalysis::IsPromptPhoton(particle);//search prompt photons among all truth particles              
      double phetisoEDcorr = 0;
      if(IsPromptph){
	if(particle->pt()/1000.>lpphotonet){
	  hayph = true;
	  particle_itr_lpphoton = truth_part;
	  lpphotone = particle->e()/1000.;
	  lpphotonet = particle->pt()/1000.;
	  lpphotonetparton = photonAnalysis::GetPartonPt(particle); // already divided by 1000
	  lpphotoneta = particle->eta();
	  lpphotonabseta = fabs(lpphotoneta);
	  lpphotonphi = particle->phi();	  
	  if( photonAnalysis::IsHardPhoton(particle)) lpphotonishard = 0; //distingue entre brem y hard photons            
	  else lpphotonishard = 1;
	  if( photonAnalysis::IsUnConvTruthPhoton(particle)) lpphotonisunconv = 0; //true if unconverted;                  
	  else lpphotonisunconv = 1;
	}
      }
    }
    
    lpphotonetisoEDcorr = GetParticleIsolation((*particle_itr_lpphoton),0.4); //aislamiento para R=0.4     */
    lpphotonetisoEDcorr = lpphotonetisoEDcorr*0.001;
    if(!hayph) std::cout<<"Does not find a photon "<<std::endl;
    else{
      std::cout<<"photon features "<<lpphotonet<<" "<<lpphotoneta<<" "<<lpphotonphi<<" "<<lpphotonetisoEDcorr<< std::endl;
      std::cout<<"photon parton pt "<<lpphotonetparton<< "  "<<lpphotonishard<<std::endl;
      
    }
    h_ph_pt->Fill(lpphotonetparton,1.0);
    if(lpphotonishard == 1) h_ph_pt_brem->Fill(lpphotonetparton,1.0);
    if(lpphotonishard == 0) h_ph_pt_hard->Fill(lpphotonetparton,1.0);



    bool hayjet = false;
    double jetptmax=0;
    double jetpt2max=0;
    TLorentzVector TLVphT(0.,0.,0.,0.);
    if(hayph) TLVphT.SetPtEtaPhiE(lpphotonet, lpphotoneta, lpphotonphi, lpphotone);
    for(int n=0;n<sortedJets.size();n++ ) {
      if(m_eventCounter<5){
	std::cout <<"truth jets "<< n << " " << sortedJets[n].pt() << " " << sortedJets[n].eta() << " " << sortedJets[n].phi_std() <<" "<<sortedJets[n].m()<< std::endl;
      }
      TLorentzVector TLVjets;
      TLVjets.SetPtEtaPhiM(sortedJets[n].pt(),sortedJets[n].eta(),sortedJets[n].phi_std(),sortedJets[n].m());
      double dR_phjet = DeltaR(TLVjets,TLVphT);
      if(dR_phjet <0.8){
	if(hayph) std::cout<<" This jet does not pass dR cut "<<sortedJets[n].pt()<< "  "<<dR_phjet<<std::endl;
	
	continue;
      }
      if(fabs(sortedJets[n].rapidity())> 2.37){
	if(hayph) std::cout<< "This jet does not pass rapidity cuts "<<sortedJets[n].rapidity()<<std::endl;
	continue;
      }
      //Busco leading y subleading jets                                                                    
      if ( (sortedJets[n].pt())/1000. > jetptmax ){
	hayjet = true;
	jetpt2max = jetptmax;
	sljetpt = ljetpt;
	sljete = ljete;
	sljeteta = ljeteta;
	sljetrap = ljetrap;
	sljetphi = ljetphi;
	sljetm = ljetm;
	
	jetptmax=sortedJets[n].pt()/1000.;
	ljetpt=sortedJets[n].pt()/1000.;
	ljete=sortedJets[n].e()/1000.;
	ljeteta=sortedJets[n].eta();
	ljetrap=sortedJets[n].rapidity();
	ljetphi=sortedJets[n].phi_std();
	ljetm=sortedJets[n].m()/1000.;
      }
      else if ((sortedJets[n].pt())/1000. > jetpt2max && (sortedJets[n].pt())/1000.<jetptmax){
	jetpt2max=sortedJets[n].pt()/1000.;
	sljetpt=sortedJets[n].pt()/1000.;
	sljete=sortedJets[n].e()/1000.;
	sljeteta=sortedJets[n].eta();
	sljetrap=sortedJets[n].rapidity();
	sljetphi=sortedJets[n].phi_std();
	sljetm=sortedJets[n].m()/1000.;
	
      }
    } // end for loop over jets                                                                            
    
    
    if(!hayjet)std::cout<<" not jet found "<<std::endl;
    if(hayjet ) std::cout<<" jet features "<<ljetpt <<" "<<ljeteta<<"  "<<ljetphi<<std::endl;
    if(hayph) std::cout<<"photon features "<<lpphotonet<<" "<<lpphotoneta<<" "<<lpphotonphi<<" "<<lpphotonetisoEDcorr<< std::endl;
    bool pass =false;
    TLorentzVector TLVJetLead (0.,0.,0.,0.);  
    TLVJetLead.SetPtEtaPhiM(ljetpt,ljeteta,ljetphi,ljetm);
    if(lpphotonet > 150.){
      if((fabs(lpphotoneta)<1.37 || (fabs(lpphotoneta)<2.37 && fabs(lpphotoneta)>1.56)))
	{
	  if(ljetpt>100. )
	    {
	      
	      if(lpphotonetisoEDcorr<(4.2e-3*lpphotonet +10.)){
		std::cout<<" Leading photon "<<lpphotonet<<"  "<<lpphotoneta<<" "<<lpphotonphi<<std::endl;
		std::cout<<" Leading jet "<<ljetpt<<"  "<<ljeteta<<" "<<ljetphi<<std::endl;
		pass=true;
	      }
	      else{ std::cout<<"Fails isolation "<< lpphotonetisoEDcorr<<" cut "<<4.2e-3*lpphotonet +10.<<std::endl;}
	      
	    }
	  else{std::cout<<"Fails jetpT "<<std::endl;}
	}
      else{std::cout<<"Fails photon eta "<<std::endl;}
    }
    else{std::cout<<"Fails photon pT 25 "<<std::endl;}

  
  
  

    //average transverse energy density.                                                                   
    double rho_truth_central = 0;
    double rho_truth_forward = 0;
    
    if( (lpphotonet<min_true || lpphotonet>max_true) )//solo para Pythia, corta en los bordes de pT de cada slice                                                                                                    
      {
	ReleaseObjects();
	//return EL::StatusCode::SUCCESS;
      }
    
    //***Write Truth Tree***//                                                                             
    if(m_writeTruthTree && pass){
      m_t_runnumber = mirun;
      m_t_eventnumber = mievnt;
      m_t_mc_weight = mc_weight;
      m_t_isConvTruth = lpphotonisunconv;
      m_t_isBrem = lpphotonishard;
      m_t_isoTruth = lpphotonetisoEDcorr;
      m_t_ptTruth =lpphotonet;
      m_t_ptParton =lpphotonetparton;
      m_t_etaTruth =lpphotoneta;
      m_t_phiTruth =lpphotonphi;
      m_t_jetpt1 = ljetpt;
      m_t_jete1 = ljete;
      m_t_jetmass1 = ljetm;
      m_t_jeteta1 = ljeteta;
      m_t_jetrap1 = ljetrap;
      m_t_jetphi1 = ljetphi;
      
      m_t_jetpt2 = sljetpt;
      m_t_jete2 = sljete;
      m_t_jetmass2 = sljetm;
      m_t_jeteta2 = sljeteta;
      m_t_jetrap2 = sljetrap;
      m_t_jetphi2 = sljetphi;
      
      
      if(m_writeTruthTree) tree ("analysis")->Fill();
    }
  }
  ReleaseObjects();
  return EL::StatusCode::SUCCESS;
}


StatusCode photonAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets                                      
  // called after the last event has been processed on the worker node                                     
  // and allows you to finish up any objects you created in                                                
  // initialize() before they are written to disk.  This is actually                                       
  // fairly rare, since this happens separately for each worker node.                                      
  // Most of the time you want to do your post-processing on the                                           
  // submission node after all your histogram outputs have been                                            
  // merged.  This is different from histFinalize() in that it only                                        
  // gets called on worker nodes that processed input events.                                              

  std::cout << " Total number of events for the sample "   << m_eventCounter << std::endl;

  DeleteObjects();

  return EL::StatusCode::SUCCESS;
}





StatusCode photonAnalysis :: SetupHistograms()
{

  
  char hist[100];
  for(int i = 0; i<=4; i++){
    sprintf(&hist[0],"h_median_truth_%i",i);
    h_median_truth[i] = new TH1D(hist, hist, 100,0.0,10.);
    wk()->addOutput(h_median_truth[i]);
  }
  h_ph_pt = new TH1D("ph_pt", "ph_pt",500,0,500);
  wk()->addOutput(h_ph_pt);
  h_ph_pt_hard = new TH1D("ph_pt_hard", "ph_pt_hard",500,0,500);
  wk()->addOutput(h_ph_pt_hard);
  h_ph_pt_brem = new TH1D("ph_pt_brem", "ph_pt_brem",500,0,500);
  wk()->addOutput(h_ph_pt_brem);

  return EL::StatusCode::SUCCESS;
}



// To be called in initialize()
StatusCode photonAnalysis :: SetupEventCounters()
{
  // security: m_event must be defined
  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events in file = %lli", m_event->getEntries() ); // print long long int
  m_eventCounter = 0;
  m_numCleanEvents = 0;

  return EL::StatusCode::SUCCESS;
}




bool photonAnalysis::IsPromptPhoton(const xAOD::TruthParticle* truth_part)
{
  bool isPrompt = false;
  if(truth_part->pdgId()==22 && truth_part->status()==1 && truth_part->barcode()<200000){
    if(truth_part->hasProdVtx()){

      const xAOD::TruthVertex* prodvtx = truth_part->prodVtx();
      unsigned int mothnum = prodvtx->nIncomingParticles();
      bool isdiq = false; //DIQUARKS                                                                                                                                     
      if(mothnum == 1){
        int pdgidmother = prodvtx->incomingParticle(0)->pdgId();
	
        { int i = pdgidmother;
          if( i == 1103 || i == 2101 || i == 2103 || i == 2203 || i == 3101 || i == 3103 ||  i == 3201 || i == 3203 || i == 3303 || i == 4101 || i == 4103 || i== 4201 || i == 4203 || i == 4301 || i == 4303 || i == 4403 || i == 5101 || i== 5103 ||i == 5201 || i == 5203 || i == 5301 || i == 5303 || i == 5401 || i == 5403 ||i== 5503 ){ isdiq=true;}
        }
        if((abs(pdgidmother)>100) && (!isdiq)){
	  //The mother is a hadron        
	  //std::cout<<"the mother is a hadron "<<std::endl;
        }else{
          isPrompt = true;
	}
      }else{
	//std::cout<<"severalmothers "<<std::endl;
        //Photon with several mothers                                                                                                                                    
        bool allhadrons = true;
        //bool allpartons = true;
        for(unsigned int imoth=0;imoth<mothnum;imoth++){
          int pdgidmother = prodvtx->incomingParticle(imoth)->pdgId();
          bool isdiq = false;
          {int i = pdgidmother;
            if( (i == 1103 || i == 2101 || i == 2103 || i == 2203 || i == 3101 || i == 3103 ||
		 i == 3201 || i == 3203 || i == 3303 || i == 4101 || i == 4103 || i == 4201 ||
		 i == 4203 || i == 4301 || i == 4303 || i == 4403 || i == 5101 || i == 5103 ||
		 i == 5201 || i == 5203 || i == 5301 || i == 5303 || i == 5401 || i == 5403 ||
                 i == 5503) ){ isdiq=true;}
          }
          if(abs(pdgidmother)>100 && !isdiq){
	    //  allpartons = false;
	    // std::cout<<"mother is hadron and no diquark "<<std::endl;

          }else{
            allhadrons = false;
	    //std::cout<<" not all  ar hadrons "<<std::endl;
          }
        }//end loop over mothers                                                                                                                                         
	
        if(allhadrons){
          //Mothers are hadrons                                                                                                                                          
	  //std::cout<<" all mothers are hadrons "<<std::endl;

        }else{     
          isPrompt = true;
	  //std::cout<<" is prompt photon "<<std::endl;

        }// end not all mother hadrons                                                                                                                                   
      }
    }//end if has production vertex 
    
  } //end pdgid==22 and status=1                                                                                                                                         
  return isPrompt;
}



float photonAnalysis::GetParticleIsolation(const xAOD::TruthParticle* truth_part, float cone_size)
{
  // loop over the particles in the container                                               
  const double pi = TMath::Pi();                                                                            
  xAOD::TruthParticleContainer::const_iterator particle_itr = m_truth_particles->begin();
  xAOD::TruthParticleContainer::const_iterator particle_end = m_truth_particles->end();
  //building my jets                                                                                                                                                    
  std::vector <fastjet::PseudoJet> fjInputs;
  fjInputs.resize(0);
  for( particle_itr = m_truth_particles->begin(); particle_itr != particle_end; ++particle_itr ) {
    if( ((*particle_itr)->status())==1 && (*particle_itr)->barcode()<200000 ){
      int idpart=abs(((*particle_itr)->pdgId()));
      bool munu=false;
      if(idpart==12 || idpart==13 || idpart==14 || idpart==16){munu=true;}
      double partpx=double(((*particle_itr)->px()));
      double partpy=double(((*particle_itr)->py()));
      double partpz=double(((*particle_itr)->pz()));
      double parte =double(((*particle_itr)->e()));
      if(fabs(((*particle_itr)->eta()))<5.0 && !munu){
	fjInputs.push_back(fastjet::PseudoJet(partpx,partpy,partpz,parte));
      }
    }
  }
  
  //Run Fastjet algorithm without munu
  //Computation of the ambient ED at truth level
  //using Kt and voronoi area.
  double RparamKT = 0.5;
  fastjet::Strategy               strategy = fastjet::Best;
  fastjet::RecombinationScheme    recombScheme = fastjet::E_scheme;
  fastjet::JetDefinition          jetDefKT = fastjet::JetDefinition(fastjet::kt_algorithm, RparamKT, recombScheme, strategy);
  double effRfact=1.0;
  fastjet::AreaDefinition area_defKT(fastjet::voronoi_area,fastjet::VoronoiAreaSpec(effRfact));
  
  std::vector <fastjet::PseudoJet> inclusiveJetsKT, sortedJetsKT; inclusiveJetsKT.clear(); sortedJetsKT.clear();
  fastjet::ClusterSequenceArea clustSeqKT(fjInputs, jetDefKT, area_defKT);
  inclusiveJetsKT = clustSeqKT.inclusive_jets(0.0);
  sortedJetsKT    = sorted_by_pt(inclusiveJetsKT);
  if(m_eventCounter==1){
    std::cout << " jetdef and area_def for bkgd " << std::endl;
    std::cout << jetDefKT.description() << std::endl;
    std::cout << area_defKT.description() << std::endl;
    std::cout << " jetdef and area_def for bkgd2 " << std::endl;
    std::cout << jetDefKT.description() << std::endl;
    std::cout << area_defKT.description() << std::endl;
  }
  //Fill vectors to select the median
  double mediana1=0.0,mediana2=0.0,mediana3=0.0,mediana4=0.0;
  if(sortedJetsKT.size()>0){
    std::vector<double> valoresptA1, valoresptA2, valoresptA3, valoresptA4;
    valoresptA1.resize(0); valoresptA2.resize(0); valoresptA3.resize(0); valoresptA4.resize(0);
    for(unsigned int i=0;i<sortedJetsKT.size();i++){
      double area=sortedJetsKT[i].area();
      if(area>1e-3){
	double eta=sortedJetsKT[i].eta();
	double ratio=sortedJetsKT[i].pt()/sortedJetsKT[i].area(); //ratio pt/area
	if(fabs(eta)<1.50){
	  valoresptA1.push_back(ratio);
	}else if(fabs(eta)<3.00){
	  valoresptA2.push_back(ratio);
	}else if(fabs(eta)<4.00){
	  valoresptA3.push_back(ratio);
	}else if(fabs(eta)<5.00){
	  valoresptA4.push_back(ratio);
	}
      }
    }
    if(valoresptA1.size()>0){
      sort(valoresptA1.begin(),valoresptA1.end());
      int nmed=valoresptA1.size();
      if(nmed%2==0){
	mediana1=0.5*(valoresptA1[nmed/2]+valoresptA1[(nmed-2)/2]);}
      else{
	mediana1=valoresptA1[(nmed-1)/2];}
    }
    if(valoresptA2.size()>0){
      sort(valoresptA2.begin(),valoresptA2.end());
      int nmed=valoresptA2.size();
      if(nmed%2==0){
	mediana2=0.5*(valoresptA2[nmed/2]+valoresptA2[(nmed-2)/2]);}
      else{
	mediana2=valoresptA2[(nmed-1)/2];}
    }
    if(valoresptA3.size()>0){
      sort(valoresptA3.begin(),valoresptA3.end());
      int nmed=valoresptA3.size();
      if(nmed%2==0){
	mediana3=0.5*(valoresptA3[nmed/2]+valoresptA3[(nmed-2)/2]);}
      else{
	mediana3=valoresptA3[(nmed-1)/2];}
    }
    if(valoresptA4.size()>0){
      sort(valoresptA4.begin(),valoresptA4.end());
      int nmed=valoresptA4.size();
      if(nmed%2==0){
	mediana4=0.5*(valoresptA4[nmed/2]+valoresptA4[(nmed-2)/2]);}
      else{
	mediana4=valoresptA4[(nmed-1)/2];}
    }
  }else
    {
    std::cout << " NO kt jets found" << sortedJetsKT.size() << std::endl;
    }
  float miED[5]={0.0}; //save medians for all eta ranges
  miED[0]=mediana1;
  miED[1]=mediana2;
  miED[2]=mediana3;
  miED[3]=mediana4;
  miED[4]=0.0;

  //std::cout << " Medians " << mediana1 << " " << mediana2 << " " << mediana3 << " " << mediana4 << std::endl;
  TLorentzVector momtot(0.,0.,0.,0);
  int ietabin = -1;
  
  if(fabs(truth_part->eta())<1.50){ietabin=0;}
  else if(fabs(truth_part->eta())<3.00){ietabin=1;}
  else if(fabs(truth_part->eta())<4.00){ietabin=2;}
  else if(fabs(truth_part->eta())<5.00){ietabin=3;}
  else {ietabin=4;}
  h_median_truth[ietabin]->Fill(miED[ietabin]/1000.);

  double prompt_eta = truth_part->eta();
  double prompt_phi = truth_part->phi();
  for( particle_itr = m_truth_particles->begin(); particle_itr != particle_end; ++particle_itr ) {
    if(*particle_itr == truth_part){}
    else{
      if( ((*particle_itr)->status())==1 && (*particle_itr)->barcode()<200000){
	int idpart=abs(((*particle_itr)->pdgId()));
	bool munu=false;
	if(idpart==12 || idpart==13 || idpart==14 || idpart==16){munu=true;} //identify muons and neutrinos to not take them into account for the isolation
	if(!munu){
	  double deltar=sqrt(2.0*(cosh(((*particle_itr)->eta())-prompt_eta)-cos(((*particle_itr)->phi())-prompt_phi)));
	  if(deltar<cone_size){
	    TLorentzVector mompart(0.,0.,0.,0.);
	    mompart.SetPtEtaPhiM(double(((*particle_itr)->pt())),double(((*particle_itr)->eta())), double(((*particle_itr)->phi())),double(((*particle_itr)->m())));
	    momtot=momtot+mompart; //sum of moments of particles inside the cone of radius R=0.4
	  }
	}
      }
    }
  }
  
  float rawetiso =  momtot.Et();
  float correction = cone_size*cone_size*pi*miED[ietabin]; //correction for UE effect
  float etiso = rawetiso - correction;
  

  return etiso;
}

bool photonAnalysis::IsUnConvTruthPhoton(const xAOD::TruthParticle* truth_part)
{
  bool is_trueUnConv = true;
  float Rconv = 0.0;
  if(truth_part->hasDecayVtx()){
    //unsigned int ndough = truth_part->decayVtx()->nOutgoingParticles();
    Rconv = truth_part->decayVtx()->perp();
    if(Rconv < 800){
      is_trueUnConv = false;
    }
  }
  return is_trueUnConv;
}

bool photonAnalysis::IsHardPhoton(const xAOD::TruthParticle* truth_part)
{

  bool isHardflag = false;
  if(IsPromptPhoton(truth_part)){
    
    const xAOD::TruthParticle* mother;
    mother = truth_part->prodVtx()->incomingParticle(0);
    int mother_id=mother->pdgId();
    int mother_st=mother->status();
    if(mother_id==22 && mother_st==23){
      isHardflag = true;
    }else if( (abs(mother_id)<=6 && abs(mother_id)>=1) || mother_id==21){
      isHardflag = false;
      
    }else{
      //loop over ancestors    
      for(unsigned int ipa=0;ipa<100;ipa++){
	if(mother->hasProdVtx()){
	  unsigned int nparents=mother->prodVtx()->nIncomingParticles();
	  mother=mother->prodVtx()->incomingParticle(0);
	  int gmother_id=mother->pdgId();
	  int gmother_st=mother->status();
	  if(gmother_id==22 && gmother_st==23){
	    isHardflag = true;
	    break;
	  }
	  if( (abs(gmother_id)<=6 && abs(gmother_id)>=1) || gmother_id==21){
	    isHardflag = false;
	    break;
	    
	  }
	}
      }//end loop over ancestors
      
    }
  }
  return isHardflag;
}

double photonAnalysis::GetPartonPt(const xAOD::TruthParticle* truth_part)
{

  double partonpt = 0.0;
  if(IsPromptPhoton(truth_part)){
    
    const xAOD::TruthParticle* mother;
    mother = truth_part->prodVtx()->incomingParticle(0);
    int mother_id=mother->pdgId();
    int mother_st=mother->status();
    if(mother_st==23){
      partonpt = mother->pt()/1000.;
    }else{
      //loop over ancestors    
      for(unsigned int ipa=0;ipa<100;ipa++){
	if(mother->hasProdVtx()){
	  unsigned int nparents=mother->prodVtx()->nIncomingParticles();
	  mother=mother->prodVtx()->incomingParticle(0);
	  int gmother_id=mother->pdgId();
	  int gmother_st=mother->status();
	  if( gmother_st==23){
	    partonpt = mother->pt()/1000.;
	    break;
	  }
	}
      }//end loop over ancestors
      
    }
  }
  return partonpt;
}

// To be called at the very begining of execute()
StatusCode photonAnalysis :: Initialize_event()
{
  const char* APP_NAME = "Initialize_event";

  // Print every 1000 events
  //if( (m_eventCounter % 1000) ==0 ) Info("execute()", "Event number processed = %i", m_eventCounter );
  // m_eventCounter++;

  // Initialize baseclass members
  m_isMC = 0;
  //m_nPhotons_preselec = 0;
  m_nPhotons_prompt_truth = 0;
  m_rho_central_truth = 0;
  m_rho_forward_truth = 0;

  // Retrieve event information
  m_eventinfo = 0;
  m_event->retrieve( m_eventinfo, "EventInfo");
  if(m_eventinfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) m_isMC = true;

 // isMC?
  /*m_averageIntPerXing = m_eventinfo->averageInteractionsPerCrossing(); // averageIntPerXing
  m_actualIntPerXing = m_eventinfo->actualInteractionsPerCrossing(); // averageIntPerXing
  m_flag_pass_grl = ( m_isMC ? 1 : m_grl->passRunLB(*m_eventinfo) ); // pass GRL
  m_flag_pass_quality = (m_isMC ? 1 : ( !(m_eventinfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error) &&
                                  !(m_eventinfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error) &&
                                  !(m_eventinfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error) &&
                                  !(m_eventinfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) ) ); // pass quality
				  if(m_flag_pass_grl && m_flag_pass_quality) m_numCleanEvents++; // clean events*/


  m_central_eventShape_truth = 0;
  m_forward_eventShape_truth = 0;
  m_truth_particles = 0;
   m_egamma_truth = 0;
  m_truth_jets = 0;
  // m_truth_WZjets = 0;
  if(m_isMC)
      {
	// Retrieve TruthEventShape - central
	//m_event->retrieve( m_central_eventShape_truth, "TruthIsoCentralEventShape" );
		
	// Retrieve TruthEventShape - forward
	
	//m_event->retrieve( m_forward_eventShape_truth, "TruthIsoForwardEventShape" );
		
	//Retrieve truth particles
	m_event->retrieve(m_truth_particles, "TruthParticles");
	
	//m_event->retrieve( m_egamma_truth, "egammaTruthParticles" );
	//m_event->retrieve( m_egamma_truth, "TruthPhotons" );
	
	//m_event->retrieve(m_truth_jets, "AntiKt4TruthDressedWZJets");
		
	//m_event->retrieve(m_truth_WZjets, "AntiKt4TruthWZJets");
	
	
      }

  return EL::StatusCode::SUCCESS; 
} 






//To be called in execute before return statement()
StatusCode photonAnalysis :: ReleaseObjects()
{

 
  return EL::StatusCode::SUCCESS;
}



//To be called in finalize()
StatusCode photonAnalysis :: ShowStats()
{
  Info("finalize()", "Number of total events = %i", m_eventCounter);
  Info("finalize()", "Number of clean events = %i", m_numCleanEvents);
  return EL::StatusCode::SUCCESS;
}


//To be called in finalize()
StatusCode photonAnalysis :: DeleteObjects()
{
  std::cout<<"Deleting Objects "<<std::endl;

  return EL::StatusCode::SUCCESS;
}

