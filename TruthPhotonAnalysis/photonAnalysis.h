#ifndef STDM2ROOTAnalysis_photonAnalysis_H
#define STDM2ROOTAnalysis_photonAnalysis_H

#include <EventLoopAlgs/AlgSelect.h>
#include <EventLoop/Algorithm.h>
#include <AnaAlgorithm/AnaAlgorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODTruth/TruthParticleContainer.h"


// Photons include(s):                                                                                                                     
#include "xAODEgamma/PhotonContainer.h"
//Jet includes                    
#include "xAODJet/JetContainer.h"

// Event info and shape include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventShape/EventShape.h"

//Primary vertices
#include "xAODTracking/VertexContainer.h"
#include "AsgTools/AnaToolHandle.h"
//#include "ElectronPhotonSelectorTools/AsgDeadHVCellRemovalTool.h"
// CP include(s):

//Root includes
#include <TLorentzVector.h>
#include <TH1.h>
#include <TTree.h>
//class JetCalibrationTool;
//class JetCleaningTool;


class photonAnalysis : public EL::AnaAlgorithm
{

 public:

  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  photonAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;


  // Systematics
  bool apply_calibration_syst;
  bool doZjetselection;
  // Cuts (preselection)
  float min_et; 
  float min_jet_et; 
  float min_crack;
  float max_crack;
  float max_eta;

  // Tool options
  int FFset;

  TTree *truthTree; //!                      
  int m_eventCounter;//!                                                                                   
  bool m_writeTruthTree; //!                                                                               
  std::string outputTruthName ; //!

  double max_true; //!
  double min_true; //! 
  double mc_weight; //!  
  bool isdata; //! 
  double masscut; //! 
  double costcut;      //!                                                          

  double m_t_isConvTruth ;//!                                                                              
  double m_t_isBrem;//!                                                                                    
  double m_t_isoTruth;//!                                                                                  
  double m_t_ptTruth; //!                                                                                  
  double m_t_ptParton; //!                                                                                  
  double m_t_etaTruth;//!                                                                                  
  double m_t_phiTruth;//!                                                                                  
  int    m_t_runnumber;//!                                                                                 
  Long64_t    m_t_eventnumber;//!                                                                          
  float m_t_jete1;//!                                                                                      
  float m_t_jetpt1;//!                                                                                     
  float m_t_jeteta1;//!                                                                                    
  float m_t_jetrap1;//!                                                                                    
  float m_t_jetphi1;//!                                                                                    
  float m_t_jetmass1;//!                                                                                   

  float m_t_jete2;//!                                                                                      
  float m_t_jetpt2;//!                                                                                     
  float m_t_jeteta2;//!                                                                                    
  float m_t_jetrap2;//!                                                                                    
  float m_t_jetphi2;//!                                                                                    
  float m_t_jetmass2;//!                                                                                   
  float m_t_mc_weight;//!                                                                                  
  std::vector<double> m_t_mc_allweights;//!  


 protected:

  //********list of other class members********//
  xAOD::TEvent* m_event; //!

  // xAOD containers to retrieve and associated copies
  const xAOD::EventInfo* m_eventinfo; //!

  const xAOD::JetContainer* m_truth_jets; //!
  const xAOD::JetContainer* m_truth_WZjets; //!

  const xAOD::EventShape *m_central_eventShape_truth; //! 
  const xAOD::EventShape *m_forward_eventShape_truth; //! 

  const xAOD::TruthParticleContainer* m_truth_particles;//!;
  const xAOD::TruthParticleContainer* m_egamma_truth; //!

  // Event
  int m_numCleanEvents; //! 
  bool m_isMC; //!
  double m_rho_central_truth; //!
  double m_rho_forward_truth; //!
  int  m_nPhotons_prompt_truth; //!


  // Histograms
  TH1D* h_median_truth[5]; //!
  TH1D* h_ph_pt; //!
  TH1D* h_ph_pt_brem; //!
  TH1D* h_ph_pt_hard; //!
  //********functions for the photon analysis********//
  StatusCode Initialize_event() ;
  StatusCode SetupHistograms() ;
  StatusCode SetupEventCounters() ;
  StatusCode ShowStats() ;
  StatusCode ReleaseObjects();
  StatusCode DeleteObjects();
  
  bool IsPromptPhoton(const xAOD::TruthParticle *);
  double GetPartonPt(const xAOD::TruthParticle *);
  bool IsHardPhoton(const xAOD::TruthParticle *);
  bool IsUnConvTruthPhoton(const xAOD::TruthParticle *);
  float GetParticleIsolation(const xAOD::TruthParticle *, float cone_size);

  //auxiliary function
  const double pi =  TMath::Pi();
  double DeltaPhi(TLorentzVector one, TLorentzVector two){
    double result = 100.;
    if(!(one.Pt()<1. ||two.Pt()< 1.)) {
      double dphi = fabs(one.Phi()-two.Phi()); if (dphi > pi) dphi = 2.*pi-dphi;
      result =  dphi;
    }
    return result;
  };//!
  double DeltaEta(TLorentzVector one, TLorentzVector two){
    double result = 100.;
      double deta = fabs(one.Eta()-two.Eta()); 
      result =  deta;
    return result;
  };//!
  double DeltaRap(TLorentzVector one, TLorentzVector two){
    double result = 100.;
      double drap = fabs(one.Rapidity()-two.Rapidity()); 
      result =  drap;
    return result;
  };//!
  double CosTheta(TLorentzVector one, TLorentzVector two){
    double result = 100.;
    double cost = fabs(tanh((one.Rapidity()-two.Rapidity())/2)); 
      result =  cost;
    return result;
  };//!
  double MassPair(TLorentzVector one, TLorentzVector two){
    double result = -999.;
    double e2 = one.E()+two.E();
    double px2 = one.Px()+two.Px();
    double py2 = one.Py()+two.Py();
    double pz2 = one.Pz()+two.Pz();

    double mass = pow(e2,2)-pow(px2,2)-pow(py2,2)-pow(pz2,2); 
    if(mass> 0)result = sqrt( mass);
    else result = 0;
    return result;
  };//!
  double DeltaR(TLorentzVector one, TLorentzVector two){
    double result = 100.;
    if(!(one.Pt()<1. ||two.Pt()< 1.)) {
      double dphi = 0; double deta = 0;
      dphi = DeltaPhi(one,two);
      deta = DeltaEta(one,two);
      result = sqrt(pow(dphi,2)+pow(deta,2));
    }
    return result;
  };//!
  
  /********to leave as it is********/
  // this is a standard constructor

  // these are the functions inherited from Algorithm
  // pure virtual for the base class
  // this is needed to distribute the algorithm to the workers
  //  ClassDef(photonAnalysis, 1);
};

#endif
